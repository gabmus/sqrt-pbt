package org.gabmus.sqrtpbt;

public class MyMath {
    private static final int MAX_ITERATIONS = 10000;
    public static int error(int guessRoot, int square) {
        return Math.abs(guessRoot*guessRoot-square);
    }
    public static int sqrt2(int arg) {
        int guess = 1;
        int count = 0;
        while ((error(guess, arg) >1)) {
            int q = arg/guess;
            guess = (guess+q) /2;
            count += 1;
            if (count > MAX_ITERATIONS) break;
        }
        int delta = error(guess, arg);
        if (error(guess-1, arg) < delta) return guess-1;
        if (error(guess+1, arg) < delta) return guess+1;
        return guess;
    }

    public static long error3(long guessRoot, long square) {
        return Math.abs(guessRoot*guessRoot-square);
    }
    public static int sqrt3(long arg) {
        long guess = 1;
        long count = 0;
        while ((error3(guess, arg) >1)) {
            long q = arg/guess;
            guess = (guess+q) /2;
            count += 1;
            if (count > MAX_ITERATIONS) break;
        }
        long delta = error3(guess, arg);
        if (error3(guess-1, arg) < delta) return (int)guess-1;
        if (error3(guess+1, arg) < delta) return (int)guess+1;
        return (int)guess;
    }
}
