package org.gabmus.sqrtpbt;

import org.assertj.core.data.Offset;
import org.junit.Test;
import org.junit.jupiter.api.Tag;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static org.quicktheories.QuickTheory.qt;
import static org.quicktheories.generators.SourceDSL.integers;

public class Sqrt2PerfectSquarePBTest {

    @Test
    @Tag("slow")
    @Tag("pbt")
    public void sqrt2WithPerfectSquares() {
        // check if we go out of bound: sqrt(max int) ~=46341
        qt().forAll(integers().between(1, 46340)).checkAssert(
                i -> {
                    assertThat(MyMath.sqrt2(i*i)).isCloseTo(i, within(1));
                }
        );
    }

    @Test
    @Tag("slow")
    @Tag("pbt")
    public void sqrt3WithPerfectSquares() {
        // sqrt3 should pass for **all** integers
        qt().forAll(integers().allPositive()).checkAssert(
                i -> {
                    long j = (long)i;
                    assertThat(MyMath.sqrt3(j*j)).isEqualTo(j);
                }
        );
    }

}
